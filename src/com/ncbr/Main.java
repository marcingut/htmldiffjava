package com.ncbr;
import htmldiff.HtmlDiff;

//import java.io.FileNotFoundException;
//import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class Main {

    public static void main(String[] args) {
        try {
            if (args.length>1) {
                Path f1_path = Paths.get(args[0]);
                Path f2_path = Paths.get(args[1]);
                String f1_content = "";
                String f2_content = "";
                if (Files.exists(f1_path)) {
                    f1_content = new String(Files.readAllBytes(f1_path));
                } else
                    throw new FileNotFoundException("File not found: " + f1_path.toString());
                if (Files.exists(f2_path)) {
                    f2_content = new String(Files.readAllBytes(f2_path));
                } else
                    throw new FileNotFoundException("File not found: " + f2_path.toString());

                HtmlDiff htmldiff = new HtmlDiff(f1_content, f2_content);
                htmldiff.setDeleteCss("delete");
                htmldiff.setInsertCss("insert");
                htmldiff.setModifiedDeleteCss("delete");
                htmldiff.setModifiedInsertCss("insert");
                htmldiff.setDeleteTag("span");
                htmldiff.setInsertTag("span");
                htmldiff.build();
/*
            try {
                PrintWriter out = new PrintWriter("filename.txt");
                out.println(htmldiff.getDifference());
                out.close();
            }
            catch (FileNotFoundException ex)
            {
                System.console().writer().println(ex.getMessage());
                System.out.println(ex.getMessage());

            }
*/
                String css = "<style>" +
                        ".insert { background-color: #aaffaa !important; }" +
                        ".delete { background-color: #ff8888 !important; text-decoration: line-through }" +
                        ".tagInsert { background-color: #007700 !important; color: #ffffff }" +
                        ".tagDelete { background-color: #770000 !important; color: #ffffff }" +
                        "</style>";

                String result = htmldiff.getDifference();
                result = result.replace("</head>", css + "</head>");
                System.out.println(result);
            }
            else
                System.out.println("Wrong count of parametrs\n"+
                        "HtmlDiff.jar oryginal_file changed_file");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
