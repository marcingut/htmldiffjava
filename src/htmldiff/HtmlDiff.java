package htmldiff;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import static java.util.Arrays.copyOf;
import static java.util.Arrays.copyOfRange;


public class HtmlDiff {

	protected String content;
	protected String oldText;
	protected String newText;
	protected String[] oldWords = new String[0];
	protected String[] newWords = new String[0];
	protected Map<String,Integer[]> wordIndices = new HashMap<String,Integer[]>();
//	protected String[] wordIndices;
	protected String encoding;
	protected String[] specialCaseOpeningTags = { "<strong[^>]*", "<b[^>]*", "<i[^>]*", "<big[^>]*", "<small[^>]*", "<u[^>]*", "<sub[^>]*", "<sup[^>]*", "<strike[^>]*", "<s[^>]*", "<p[^>]*" };
	protected String[] specialCaseClosingTags = { "</strong>", "</b>", "</i>", "</big>", "</small>", "</u>", "</sub>", "</sup>", "</strike>", "</s>", "</p>" };

    protected String tag_ins = "ins";
    protected String tag_del = "del";

    protected String del_css_class = "diffdel";
    protected String ins_css_class = "diffins";

    protected String mod_del_css_class = "diffmod";
    protected String mod_ins_css_class = "diffmod";

    public HtmlDiff( String oldText, String newText) {
		this(oldText,newText, "UTF-8");
	}

	public HtmlDiff( String oldText, String newText, String encoding ) {
		this.oldText = this.purifyHtml( oldText.trim() );
		this.newText = this.purifyHtml( newText.trim() );
		this.encoding = encoding;
		this.content = "";
	}

    public void setModifiedInsertCss(String cssClass)
    {
        this.mod_ins_css_class = cssClass;
    }

    public String getModifiedInsertCss()
    {
        return this.mod_ins_css_class;
    }

    public void setModifiedDeleteCss(String cssClass)
    {
        this.mod_del_css_class = cssClass;
    }

    public String getModifiedDeleteCss()
    {
        return this.mod_del_css_class;
    }

    public void setInsertCss(String cssClass)
    {
        this.ins_css_class = cssClass;
    }

    public String getInsertCss()
    {
        return this.ins_css_class;
    }

    public void setDeleteCss(String cssClass)
    {
        this.del_css_class = cssClass;
    }

    public String getDeleteCss()
    {
        return this.del_css_class;
    }

    public void setInsertTag(String tagName)
    {
        this.tag_ins = tagName;
    }

    public String getInsertTag()
    {
        return this.tag_ins;
    }

    public void setDeleteTag(String tagName)
    {
        this.tag_del = tagName;
    }

    public String getDeleteTag()
    {
        return this.tag_del;
    }

	public String getOldHtml() {
		return this.oldText;
	}

	public String getNewHtml() {
		return this.newText;
	}

	public String getDifference() {
		return this.content;
	}

	protected String getStringBetween( String str, String start, String end ) {
		String[] expStr = str.split(start,2);
		if( expStr.length > 1 ) {
			expStr = expStr[ 1 ].split(end);
			if( expStr.length > 1 ) {
				expStr = copyOf(expStr, expStr.length-1);
				return String.join( end, expStr );
			}
		}
		return "";
	}

	protected String purifyHtml( String html ) {
		return html;
	}

	public String build() {
		this.SplitInputsToWords();
		this.IndexNewWords();
		Operation[] operations = this.Operations();

		for( Operation item : operations ) {
			this.PerformOperation( item );
		}

		return this.content;
	}

	protected void IndexNewWords() {
		for( int i = 0,n= this.newWords.length;i <n ;i++ ) {
			String word = this.newWords[i];

			if( this.IsTag( word ) ) {
				word = this.StripTagAttributes( word );
			}
			if( this.wordIndices.containsKey(word) ) {
				Integer[] curr_int = this.wordIndices.get(word);

				curr_int = copyOf(curr_int,curr_int.length+1);
				curr_int[curr_int.length-1] = i;
				this.wordIndices.replace(word, curr_int);
			} else {
				Integer[] curr_int = {i};
				this.wordIndices.put(word, curr_int);
			}
		}
	}

	protected void SplitInputsToWords() {
		this.oldWords = this.ConvertHtmlToListOfWords( this.Explode( this.oldText ) );
		this.newWords = this.ConvertHtmlToListOfWords( this.Explode( this.newText ) );
	}

	protected String [] ConvertHtmlToListOfWords( String [] characterString ) {
		String mode = "character";
		String current_word = "";
		String[] words = new String[0];

		for( String character : characterString ) {

			switch ( mode ) {
				case "character":
					if( this.IsStartOfTag( character ) ) {
						if( !current_word.equals("") ) {
							words = copyOf(words,words.length+1);
							words[words.length-1] = current_word;
						}
						current_word = "<";
						mode = "tag";
					} else if( !Pattern.compile("[^\\s]").matcher(character).find()  ) {
						if( !current_word.equals("") ) {
							words = copyOf(words,words.length+1);
							words[words.length-1] = current_word;
						}
						current_word = character;
						mode = "whitespace";
					} else {
						if( this.IsAlphaNum( character ) && ( current_word.length() == 0 || this.IsAlphaNum( current_word ) ) ) {
							current_word += character;
						} else {
							words = copyOf(words,words.length+1);
							words[words.length-1] = current_word;
							current_word = character;
						}
					}
					break;
				case "tag" :
					if( this.IsEndOfTag( character ) ) {
						current_word += ">";
						words = copyOf(words,words.length+1);
						words[words.length-1] = current_word;
						current_word = "";

						if( Pattern.compile("[^\\s]").matcher(character).find() ) {
							mode = "whitespace";
						} else {
							mode = "character";
						}
					} else {
						current_word += character;
					}
					break;
				case "whitespace":
					if( this.IsStartOfTag( character ) ) {
						if( !current_word.equals("") ) {
							words = copyOf(words,words.length+1);
							words[words.length-1] = current_word;
						}
						current_word = "<";
						mode = "tag";
					} else if( !Pattern.compile("[^\\s]").matcher(character).find() ) {
						current_word += character;
					} else {
						if( !current_word.equals("") ) {
							words = copyOf(words,words.length+1);
							words[words.length-1] = current_word;
						}
						current_word = character;
						mode = "character";
					}
					break;
				default:
					break;
			}
		}
		if( !current_word.equals("") ) {
			words = copyOf(words,words.length+1);
			words[words.length-1] = current_word;
		}
		return words;
	}

	protected boolean IsStartOfTag( String val ) {
		return val.equals("<");
	}

	protected boolean IsEndOfTag( String val ) {
		return val.equals(">");
	}

	protected boolean IsWhiteSpace( String value ) {
		return !Pattern.compile("\\S").matcher(value).find();
	}

	protected boolean IsAlphaNum( String value ) {
		return Pattern.compile("[\\p{L}\\p{N}]+").matcher(value).find();
	}

	protected String[] Explode( String value ) {
		// as suggested by @onassar
		return value.split("");
	}

	protected void PerformOperation( Operation operation ) {
		switch( operation.Action ) {
			case "equal" :
				this.ProcessEqualOperation( operation );
				break;
			case "delete" :
				this.ProcessDeleteOperation( operation, this.del_css_class );
				break;
			case "insert" :
				this.ProcessInsertOperation( operation, this.ins_css_class);
				break;
			case "replace":
				this.ProcessReplaceOperation( operation );
				break;
			default:
				break;
		}
	}

	protected void ProcessReplaceOperation( Operation operation ) {
		this.ProcessDeleteOperation( operation, this.mod_del_css_class );
		this.ProcessInsertOperation( operation, this.mod_ins_css_class );
	}

	protected void ProcessInsertOperation( Operation operation, String cssClass ) {
		String[] text = new String[0];
		for( int pos=0,n=this.newWords.length;pos<n;pos++) {
			String s=this.newWords[pos];
			if( pos >= operation.StartInNew && pos < operation.EndInNew ) {
				text = copyOf(text,text.length+1);
				text[text.length-1] = s;
			}
		}
		this.InsertTag( this.tag_ins, cssClass, text );
	}

	protected void ProcessDeleteOperation( Operation operation, String cssClass ) {
		String[] text = new String[0];
		for( int pos=0,n=this.oldWords.length;pos<n;pos++) {
			String s=this.oldWords[pos];
			if( pos >= operation.StartInOld && pos < operation.EndInOld ) {
				text = copyOf(text,text.length+1);
				text[text.length-1] = s;
			}
		}
		this.InsertTag( this.tag_del, cssClass, text );
	}

	protected void ProcessEqualOperation( Operation operation ) {
		String[] result = new String[0];
		for( int pos=0,n=this.newWords.length;pos<n;pos++) {
			String s=this.newWords[pos];
			if( pos >= operation.StartInNew && pos < operation.EndInNew ) {
				result = copyOf(result,result.length+1);
				result[result.length-1] = s;
			}
		}
		this.content += String.join("",result);
	}

	protected void InsertTag( String tag, String cssClass, String[] words ) {
		AtomicReference<String[]> words_ref = new AtomicReference<String[]>(words);
		while( true ) {
			if( words.length == 0 ) {
				break;
			}
			words_ref.set(words);
			String[] nonTags = this.ExtractConsecutiveWords( words_ref, "noTag" );
			words = words_ref.get();

			String specialCaseTagInjection = "";
			boolean specialCaseTagInjectionIsBefore = false;

			if( nonTags.length != 0 ) {
				String text = this.WrapText( String.join("", nonTags), tag, cssClass );
				this.content += text;
			} else {
				String firstOrDefault = null;
				for( String x : this.specialCaseOpeningTags ) {
					if( Pattern.compile(x).matcher(words[0]).find() ) {
						firstOrDefault = x;
						break;
					}
				}
				if( firstOrDefault != null ) {
					specialCaseTagInjection = "<ins class=\"mod\">";
					if( tag.equals("del") ) {
						words = copyOfRange(words,1,words.length);//unset
					}
				} else {
					boolean found = false;
					for (String elem : this.specialCaseClosingTags)
					{
						if (words[ 0 ].equals(elem)) found = true;
					}

					if (found) {
						specialCaseTagInjection = "</ins>";
						specialCaseTagInjectionIsBefore = true;
						if (tag.equals("del")) {
							words = copyOfRange(words,1,words.length);//unset
						}
					}
				}
			}
			if( words.length == 0 && specialCaseTagInjection.length() == 0 ) {
				break;
			}
			if( specialCaseTagInjectionIsBefore ) {
				words_ref.set(words);
				this.content += specialCaseTagInjection + String.join( "", this.ExtractConsecutiveWords( words_ref, "tag" ) );
				words = words_ref.get();
			} else {
				words_ref.set(words);
				String[] workTag = this.ExtractConsecutiveWords( words_ref, "tag" );
				words = words_ref.get();

		        if( workTag.length > 0 && this.IsOpeningTag( workTag[ 0 ] ) && !this.IsClosingTag( workTag[ 0 ] ))  {
		            if( workTag[ 0 ].indexOf("class=") > -1) {
		                workTag[ 0 ] = workTag[ 0 ].replace( "class=\"", "class=\"diffmod " );
		                workTag[ 0 ] = workTag[ 0 ].replace( "class='", "class=\"diffmod ");
		            } else {
		                workTag[ 0 ] = workTag[ 0 ].replace( ">", " class=\"diffmod\">" );
		            }
		        }
		        this.content += String.join( "", workTag ) + specialCaseTagInjection;
			}
		}
	}

	protected boolean checkCondition( String word, String condition ) {
		return condition.equals("tag") ? this.IsTag( word ) : !this.IsTag( word );
	}

	protected String WrapText( String text, String tagName, String cssClass ) {
		return String.format( "<%1$s class=\"%2$s\">%3$s</%1$s>", tagName, cssClass, text );
	}

	protected String[] ExtractConsecutiveWords(AtomicReference<String[]> /*&*/words_ref, String condition ) {
		Integer indexOfFirstTag = null;
		String[] words = words_ref.get();
		for( int i = 0, n=words.length; i<n;i++ ) {
			String word = words[i];
			if( !this.checkCondition( word, condition ) ) {
				indexOfFirstTag = i;
				break;
			}
		}
		if( indexOfFirstTag != null ) {
			String[] items = new String[0];
			for( int pos = 0, n=words.length; pos<n;pos++ ) {
				String s = words[pos];
			    if( pos >= 0 && pos < indexOfFirstTag ) {
					items = copyOf(items,items.length+1);
					items[items.length-1] = s;
				}
			}
			if( indexOfFirstTag > 0 ) {
				words = copyOfRange(words,indexOfFirstTag,words.length);
			}
			words_ref.set(words);
			return items;
		} else {
			String[] items = new String[0];
			for( int pos = 0, n=words.length; pos<n;pos++ ) {
				String s = words[pos];
				if( pos >= 0 && pos <= words.length ) {
					items = copyOf(items,items.length+1);
					items[items.length-1] = s;
				}
			}

			words = new String[0];

			words_ref.set(words);
			return items;
		}
	}

	protected boolean IsTag( String item ) {
		return this.IsOpeningTag( item ) || this.IsClosingTag( item );
	}

	protected boolean IsOpeningTag( String item ) {
		Pattern p = Pattern.compile("<[^>]+?>\\s*?", Pattern.MULTILINE|Pattern.CASE_INSENSITIVE);
		return p.matcher(item).find();
	}

	protected boolean IsClosingTag( String item )
	{
		Pattern p = Pattern.compile("</[^>]+?>\\s*?", Pattern.MULTILINE|Pattern.CASE_INSENSITIVE);
		return p.matcher(item).find();
	}

	protected Operation[] Operations() {
		int positionInOld = 0;
		int positionInNew = 0;
		Operation[] operations = new Operation[0];
		Match[] matches = this.MatchingBlocks();

		matches = copyOf(matches,matches.length+1);
		matches[matches.length-1]= new Match( this.oldWords.length, this.newWords.length,0 );

		for( Match match : matches ) {
			boolean matchStartsAtCurrentPositionInOld = ( positionInOld == match.StartInOld );
			boolean matchStartsAtCurrentPositionInNew = ( positionInNew == match.StartInNew );
			String action = "none";

			if(!matchStartsAtCurrentPositionInOld && !matchStartsAtCurrentPositionInNew) {
				action = "replace";
			} else if(matchStartsAtCurrentPositionInOld && !matchStartsAtCurrentPositionInNew) {
				action = "insert";
			} else if(!matchStartsAtCurrentPositionInOld && matchStartsAtCurrentPositionInNew ) {
				action = "delete";
			} else { // This occurs if the first few words are the same in both versions
				action = "none";
			}
			if( !action.equals("none") ) {
				operations = copyOf(operations,operations.length+1);
				operations[operations.length-1] =new Operation( action, positionInOld, match.StartInOld, positionInNew, match.StartInNew );
			}
			operations = copyOf(operations,operations.length+1);
			operations[operations.length-1] = new Operation( "equal", match.StartInOld, match.EndInOld(), match.StartInNew, match.EndInNew() );

			positionInOld = match.EndInOld();
			positionInNew = match.EndInNew();
		}
		return operations;
	}

	protected Match[] MatchingBlocks() {
		Match[] matchingBlocks = new Match[0];
		matchingBlocks = this.FindMatchingBlocks( 0, this.oldWords.length, 0, this.newWords.length, matchingBlocks );
		return matchingBlocks;
	}

	protected Match[] FindMatchingBlocks( int startInOld, int endInOld, int startInNew, int endInNew, Match[] matchingBlocks ) {
		Match match = this.FindMatch(startInOld, endInOld, startInNew, endInNew);
		if( match != null ) {
			if( startInOld < match.StartInOld && startInNew < match.StartInNew ) {
				matchingBlocks = this.FindMatchingBlocks( startInOld, match.StartInOld, startInNew, match.StartInNew, matchingBlocks );
			}

			matchingBlocks  = copyOf(matchingBlocks,matchingBlocks.length+1);
			matchingBlocks[matchingBlocks.length-1] = match;

			if( match.EndInOld() < endInOld && match.EndInNew() < endInNew ) {
				matchingBlocks = this.FindMatchingBlocks( match.EndInOld(), endInOld, match.EndInNew(), endInNew, matchingBlocks );
			}
		}
		return matchingBlocks;
	}

	protected String StripTagAttributes( String word ) {
		String[] words = Pattern.compile("(^<|>$)").matcher(word).replaceAll("").split(" ");
		return "<"+ words[ 0 ] + ">";
	}

	protected Match FindMatch( int startInOld, int endInOld, int startInNew, int endInNew ) {
		int bestMatchInOld = startInOld;
		int bestMatchInNew = startInNew;
		int bestMatchSize = 0;
        Map<Integer,Integer> matchLengthAt = new HashMap<Integer,Integer>();

		for( int indexInOld = startInOld; indexInOld < endInOld; indexInOld++ ) {
            Map<Integer,Integer> newMatchLengthAt = new HashMap<Integer,Integer>();
			String index = this.oldWords[ indexInOld ];
			if( this.IsTag( index ) ) {
				index = this.StripTagAttributes( index );
			}
			if( !this.wordIndices.containsKey(index)  ) {
				matchLengthAt = newMatchLengthAt;
				continue;
			}

            Integer[] tmp_xx = this.wordIndices.get(index);
			for( Integer indexInNew : tmp_xx ) {

				if( indexInNew < startInNew ) {
					continue;
				}
				if( indexInNew >= endInNew ) {
					break;
				}

                Integer tmp = matchLengthAt.get(indexInNew - 1);
				int newMatchLength = (((indexInNew - 1)<0)?0:(tmp==null?0:tmp)) + 1;
				newMatchLengthAt.put(indexInNew,newMatchLength);
				if( newMatchLength > bestMatchSize ) {
					bestMatchInOld = indexInOld - newMatchLength + 1;
					bestMatchInNew = indexInNew - newMatchLength + 1;
					bestMatchSize = newMatchLength;
				}
			}
			matchLengthAt = newMatchLengthAt;
		}
		Match result = null;
		if (bestMatchSize != 0)
			result = new Match( bestMatchInOld, bestMatchInNew, bestMatchSize );
		return result;
	}
}
