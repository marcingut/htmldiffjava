package htmldiff;

public class Match {

	public int StartInOld;
	public int StartInNew;
	public int Size;

	public Match( int startInOld, int startInNew, int size ) {
		this.StartInOld = startInOld;
		this.StartInNew = startInNew;
		this.Size = size;
	}

	public int EndInOld() {
		return this.StartInOld + this.Size;
	}

	public int EndInNew() {
		return this.StartInNew + this.Size;
	}
	public String toString()
	{
		return StartInOld +" "+StartInNew+" "+Size+"\n";
	}
}
