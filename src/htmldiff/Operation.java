package htmldiff;

public class Operation {

	public String Action;
	public int StartInOld;
	public int EndInOld;
	public int StartInNew;
	public int EndInNew;

	public Operation( String action, int startInOld, int endInOld, int startInNew, int endInNew ) {
		this.Action = action;
		this.StartInOld = startInOld;
		this.EndInOld = endInOld;
		this.StartInNew = startInNew;
		this.EndInNew = endInNew;
	}
}

