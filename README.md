h2. Project Description

A library for comparing two HTML files/snippets and highlighting the differences using simple HTML.

This HTML Diff implementation is a Java port of the PHP implementation found "here":https://github.com/rashid2538/php-htmldiff.

Implementation based on JDK 1.8.

h2. Command line usage

java -Dfile.encoding=UTF-8 -jar HtmlDiff.jar oryginal_file changed_file > result_file
